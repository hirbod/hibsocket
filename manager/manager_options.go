package manager

import (
	"github.com/gorilla/websocket"
)

// Opts allow passing options to the NewManager to configure the default functionality.
type Opts func(*Manager)

// WithUpgrader allows you to pass a custom upgrader instead of using the default one provided by the package.
func WithUpgrader(upgrader websocket.Upgrader) Opts {
	return func(m *Manager) {
		m.upgrader = upgrader
	}
}

// WithEventHandling allows registering of EventHandler callback functions for each defined event type.
func WithEventHandling(eventHandlers map[string]EventHandler) Opts {
	return func(m *Manager) {
		m.eventHandlers = eventHandlers
	}
}

// WithAuthenticationHandler allows registering of AuthHandler callback functions for onConnect.Authentication.
// This handler only triggers when a new connection connects to the websocket server.
// There is no authentication if this handler is not provided.
func WithAuthenticationHandler(authHandler AuthHandler) Opts {
	return func(m *Manager) {
		m.authHandler = authHandler
	}
}

// WithWebsocketConfig allows configuration of the manager and client values.
func WithWebsocketConfig(config *WebsocketConfig) Opts {
	return func(m *Manager) {
		m.config = config
	}
}
