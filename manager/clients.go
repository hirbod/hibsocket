package manager

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	proto "github.com/gravity-technologies/platform/backend/proto/go/websocket"
	"log"
	"math/rand"
	"time"
)

// Client Keeps reference to the connection to the client
type Client struct {
	connIdentifier string // TODO: discuss about the implementation of identifier.
	conn           *websocket.Conn
	mgr            *Manager
	connectTime    time.Time
	send           chan *writeMessageJob
}

// NewClient Constructor
func NewClient(conn *websocket.Conn, mgr *Manager) *Client {
	client := &Client{
		connectTime:    time.Now(),
		conn:           conn,
		mgr:            mgr,
		send:           make(chan *writeMessageJob, 256),
		connIdentifier: fmt.Sprintf("connIdentifier:%v", rand.Uint64()),
	}

	go client.readMsg()
	go client.writeMsgWorker()

	return client
}

// readMsg will run per connection. Even though all of our connections are 1 way messaging (server -> client)
// we still require to keep this for when client wants to close the connection
func (c *Client) readMsg() {
	defer func() {
		c.mgr.removeClient(c)
	}()

	for {
		msgType, payload, err := c.conn.ReadMessage()
		if err != nil {
			// Check and log for unexpected closure, otherwise we can continue reading the buffer
			if c.websocketIsUnexpectedClose(err) {
				return
			}
			fmt.Printf("failed for some reason %v", err)
			continue
		}
		// The message types are defined in RFC 6455, section 11.8.
		switch msgType {
		case websocket.CloseMessage:
			// websocket.CloseMessage is the only type that should close the readMsg worker
			// since we don't expect the client to send any more data through the websocket
			return
		case websocket.TextMessage:
			c.handleTxtMsg(payload)
		case websocket.PingMessage:
			c.handlePingMsg()
		}
	}
}

// writeMsg put the msg in the channel to be consumed by the writeMsgWorker. This will avoid any race condition
// in writing messages to the network
func (c *Client) writeMsg(msg []byte, msgType int) {
	c.send <- &writeMessageJob{
		body:        msg,
		messageType: msgType,
	}
}

// writeMsgWorker will run and consume the messages that are in the send channel
func (c *Client) writeMsgWorker() {
	defer func() {
		c.mgr.removeClient(c)
	}()

	for {
		select {
		case job, ok := <-c.send:
			// Mgr has closed the channel
			if !ok {
				// Ignore the error since we are going to close the connection after this regardless
				_ = c.conn.WriteMessage(websocket.CloseMessage, nil)
				return
			}

			// update read and write deadline
			_ = c.conn.SetReadDeadline(getDeadline(c.mgr.config.ReadDeadline))
			_ = c.conn.SetWriteDeadline(getDeadline(c.mgr.config.WriteDeadline))

			// Writes the message to the network. In case of any error we do check to validate if the connection
			// has been unexpectedly closed or not
			if err := c.conn.WriteMessage(job.messageType, job.body); err != nil {
				if c.websocketIsUnexpectedClose(err) {
					return
				}
				log.Printf("failed to write message to the client %v with err: %v", c.connIdentifier, err)
				continue
			}
		}
	}
}

// handleTxtMsg used for reading websocket.TextMessage payloads and triggering the event based on registered events
func (c *Client) handleTxtMsg(payload []byte) {
	event := &proto.WebsocketEvent{}
	if err := json.Unmarshal(payload, event); err != nil {
		log.Printf("failed to unmarshal payload message with err: %v", err)
		return
	}
	c.mgr.routeEvent(event, c)
}

// handlePingMsg returns a pong to the client
// PingPong should be used to extend the client read deadline to avoid disconnect over slow connections
func (c *Client) handlePingMsg() {
	c.writeMsg(nil, websocket.PongMessage)
}

// websocketIsUnexpectedClose returns true and logs if websocket has CloseGoingAway or CloseAbnormalClosure error
func (c *Client) websocketIsUnexpectedClose(err error) bool {
	if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
		return false
	}
	log.Printf("connection from client [%v] is unexpectly closed with err: %v ", c.connIdentifier, err)

	return true
}
