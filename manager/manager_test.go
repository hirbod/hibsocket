package manager

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	proto "github.com/gravity-technologies/platform/backend/proto/go/websocket"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func setup(opts ...Opts) (*Manager, *httptest.Server) {
	mgr := NewManager(opts...)
	srvr := httptest.NewServer(http.HandlerFunc(mgr.ServeWS))
	return mgr, srvr
}

func createWSConnection(t *testing.T, urlsString string, requestHeader http.Header) *websocket.Conn {
	ws, _, err := websocket.DefaultDialer.Dial(urlsString, requestHeader)
	assert.Nilf(t, err, "websocket failed to connect to the server with error %v", err)
	assert.NotNil(t, ws, "websocket is nil")
	return ws
}

func validateNextRead(t *testing.T, conn *websocket.Conn, expectedNextRead []byte, expectedMsgType int) {
	msgType, byteArray, err := conn.ReadMessage()
	assert.Nilf(t, err, "failed to ReadMessage with error %v", err)
	assert.Equalf(t, expectedMsgType, msgType,
		"expectedMsgType %d and msgType %d are not same", expectedMsgType, msgType)
	assert.Equalf(t, expectedNextRead, byteArray,
		"expectedNextRead %s is not same with ReadMessage %s", string(byteArray), string(expectedNextRead))
}

func TestSimpleClientConnection(t *testing.T) {
	mgr, srvr := setup()
	defer srvr.Close()

	urlsString := "ws" + strings.TrimPrefix(srvr.URL, "http")
	conn := createWSConnection(t, urlsString, nil)
	defer conn.Close()

	msgList := [][]byte{
		[]byte("first message to the connection"),
		[]byte("second message to the connection"),
		[]byte("last message to the connection"),
	}

	for _, entry := range msgList {
		mgr.Broadcast(entry)
		validateNextRead(t, conn, entry, websocket.TextMessage)
	}
}

func TestSimpleUnicast(t *testing.T) {
	mgr, srvr := setup()
	defer srvr.Close()

	urlsString := "ws" + strings.TrimPrefix(srvr.URL, "http")
	listenerConn := createWSConnection(t, urlsString, nil)
	defer listenerConn.Close()

	var connIdentifier string

	msgList := [][]byte{
		[]byte("first message to the connection"),
		[]byte("second message to the connection"),
		[]byte("last message to the connection"),
	}

	mgr.clients.Range(func(key, value any) bool {
		var ok bool
		connIdentifier, ok = key.(string)
		if !ok {
			t.Errorf("failed to get the key of type %T ", key)
		}
		assert.NotEmpty(t, connIdentifier, "connIdentifier is empty expected non-empty")
		return true
	})

	silentConn := createWSConnection(t, urlsString, nil)
	go validateNextRead(t, silentConn, []byte("invalid receiver triggered"), websocket.TextMessage)

	time.Sleep(time.Second * 1)

	for _, entry := range msgList {
		mgr.Unicast(connIdentifier, entry)
		validateNextRead(t, listenerConn, entry, websocket.TextMessage)
	}
}

func TestClientConnectionBrokenLink(t *testing.T) {
	mgr, srvr := setup()
	defer srvr.Close()

	urlsString := "ws" + strings.TrimPrefix(srvr.URL, "http")
	brokenConnection := createWSConnection(t, urlsString, nil)
	validConnection := createWSConnection(t, urlsString, nil)

	brokenConnection.Close()
	defer validConnection.Close()

	msgList := [][]byte{
		[]byte("first message to the connection"),
		[]byte("second message to the connection"),
		[]byte("last message to the connection"),
	}

	for _, entry := range msgList {
		mgr.Broadcast(entry)
		validateNextRead(t, validConnection, entry, websocket.TextMessage)
	}
}

func TestBroadcastMultipleClientConnection(t *testing.T) {
	connectionCount := 100
	mgr, srvr := setup()
	defer srvr.Close()

	urlsString := "ws" + strings.TrimPrefix(srvr.URL, "http")

	var connectionList []*websocket.Conn
	for i := 0; i < connectionCount; i++ {
		conn := createWSConnection(t, urlsString, nil)
		connectionList = append(connectionList, conn)
		defer conn.Close()
	}
	assert.Equalf(t, connectionCount, len(connectionList), "failed to create enough connections")

	msgList := [][]byte{
		[]byte("first message to the connection"),
		[]byte("second message to the connection"),
		[]byte("last message to the connection"),
	}

	for _, entry := range msgList {
		mgr.Broadcast(entry)
	}

	for _, conn := range connectionList {
		for _, entry := range msgList {
			validateNextRead(t, conn, entry, websocket.TextMessage)
		}
	}
}

func TestBroadcastMultipleClientChaosMonkeyConnectionFailure(t *testing.T) {
	connectionCount := 100
	mgr, srvr := setup()
	defer srvr.Close()

	urlsString := "ws" + strings.TrimPrefix(srvr.URL, "http")

	var connectionList []*websocket.Conn
	for i := 0; i < connectionCount; i++ {
		conn := createWSConnection(t, urlsString, nil)
		connectionList = append(connectionList, conn)
		defer conn.Close()
	}
	assert.Equalf(t, connectionCount, len(connectionList), "failed to create enough connections")

	msgList := [][]byte{
		[]byte("first message to the connection"),
		[]byte("second message to the connection"),
		[]byte("third message to the connection"),
	}

	for len(connectionList) > 0 {
		deadIdx := rand.Int() % len(connectionList)
		deadConn := connectionList[deadIdx]
		deadConn.Close()

		connectionList = append(connectionList[:deadIdx], connectionList[deadIdx+1:]...)

		for _, entry := range msgList {
			mgr.Broadcast(entry)
		}

		for _, conn := range connectionList {
			for _, entry := range msgList {
				validateNextRead(t, conn, entry, websocket.TextMessage)
			}
		}
	}
}

func getSimplehandler(t *testing.T, expectedEvent *proto.WebsocketEvent) EventHandler {
	return func(gotEvent *proto.WebsocketEvent) {
		assert.NotNilf(t, gotEvent, "gotEvent %v is nil", gotEvent)
		assert.Equalf(t, expectedEvent.GetEventType(), gotEvent.GetEventType(),
			"invalid gotEvent type. Expected %v but got %v", expectedEvent.GetEventType(), gotEvent.GetEventType())
		assert.Equalf(t, expectedEvent.GetData(), gotEvent.GetData(),
			"values from expectedData [%v] and gotData [%v] are not same", expectedEvent.GetData(), gotEvent.GetData())
	}
}

func TestSimpleEventCallback(t *testing.T) {

	eventHandlers := make(map[string]EventHandler)
	eventHandlers["event.1.trigger"] = getSimplehandler(t, &proto.WebsocketEvent{
		EventType: "event.1.trigger",
		Data:      []byte("event_1_triggered_data"),
	})
	eventHandlers["event.2.trigger"] = getSimplehandler(t, &proto.WebsocketEvent{
		EventType: "event.2.trigger",
		Data:      []byte("event_2_triggered_data"),
	})

	_, srvr := setup(WithEventHandling(eventHandlers))
	defer srvr.Close()

	urlsString := "ws" + strings.TrimPrefix(srvr.URL, "http")
	conn := createWSConnection(t, urlsString, nil)
	defer conn.Close()

	data := &proto.WebsocketEvent{
		EventType: "event.1.trigger",
		Data:      []byte("event_1_triggered_data"),
	}
	byteArray, err := json.Marshal(data)
	assert.Nilf(t, err, "failed to json.marshal with err: %v", err)

	err = conn.WriteMessage(websocket.TextMessage, byteArray)
	assert.Nilf(t, err, "failed to conn.WriteMessage with err: %v", err)

	data = &proto.WebsocketEvent{
		EventType: "event.2.trigger",
		Data:      []byte("event_2_triggered_data"),
	}
	byteArray, err = json.Marshal(data)
	assert.Nilf(t, err, "failed to json.marshal with err: %v", err)

	err = conn.WriteMessage(websocket.TextMessage, byteArray)
	assert.Nilf(t, err, "failed to conn.WriteMessage with err: %v", err)

	time.Sleep(1 * time.Second)
}

func TestMultipleClientSameOriginConnection(t *testing.T) {
	mgr, srvr := setup(WithWebsocketConfig(&WebsocketConfig{
		ReadLimitSize:         0,
		ReadDeadline:          0,
		WriteDeadline:         0,
		LimitConnectionFromIP: true,
	}))
	defer srvr.Close()

	urlsString := "ws" + strings.TrimPrefix(srvr.URL, "http")
	initialConn := createWSConnection(t, urlsString, http.Header{
		"X-FORWARDED-FOR": []string{"127.0.0.1"},
	})

	msgList := [][]byte{
		[]byte("first message to the connection"),
		[]byte("second message to the connection"),
		[]byte("last message to the connection"),
	}

	for _, entry := range msgList {
		mgr.Broadcast(entry)
		validateNextRead(t, initialConn, entry, websocket.TextMessage)
	}

	secondConn := createWSConnection(t, urlsString, http.Header{
		"X-FORWARDED-FOR": []string{"127.0.0.1"},
	})
	defer secondConn.Close()

	_, _, err := initialConn.ReadMessage()
	assert.NotNilf(t, err, "expected err  due to close connection, but got %v", err)

	for _, entry := range msgList {
		mgr.Broadcast(entry)
		validateNextRead(t, secondConn, entry, websocket.TextMessage)
	}
}

func getSimpleAuthHandler(t *testing.T, counter *int) AuthHandler {
	return func(r *http.Request) error {
		*counter += 1
		tokenString := r.Header.Get("Authorization")
		assert.NotNilf(t, tokenString, "expected a non nil tokenString but got %v", tokenString)
		return nil
	}
}

func TestClientRequireAuth(t *testing.T) {
	count := 0
	_, srvr := setup(WithAuthenticationHandler(getSimpleAuthHandler(t, &count)))
	defer srvr.Close()

	urlsString := "ws" + strings.TrimPrefix(srvr.URL, "http")
	validConn := createWSConnection(t, urlsString, http.Header{
		"Authorization": []string{"my-token"},
	})
	defer validConn.Close()

	invalidConn := createWSConnection(t, urlsString, nil)
	defer invalidConn.Close()

	time.Sleep(1 * time.Second)

	assert.Equalf(t, 2, count, "expected to call the handler 2 times, but got %v", count)
}
