package manager

import (
	"github.com/gorilla/websocket"
	proto "github.com/gravity-technologies/platform/backend/proto/go/websocket"
	"log"
	"net/http"
	"sync"
)

var (
	// Default values for upgrade to upgrade the incoming http request to a websocket connection
	defaultUpgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			// Allow all connections by default
			return true
		},
	}
)

// Manager keeps reference to all the clients to allow for emit of message and broadcasts
type Manager struct {
	clients       sync.Map // clientId -> client
	clientsOrigin sync.Map // clientIp -> client
	upgrader      websocket.Upgrader
	eventHandlers map[string]EventHandler
	authHandler   AuthHandler
	config        *WebsocketConfig
}

// NewManager Constructor
func NewManager(opts ...Opts) *Manager {
	mgr := &Manager{
		upgrader: defaultUpgrader,
		config:   defaultConfig,
	}

	for _, opt := range opts {
		opt(mgr)
	}

	return mgr
}

// ServeWS HTTP Handler that allows the HTTP connection to be upgraded to a websocket connection
// Upgrade the request. If the upgrade fails the connection will be lost and the client has to retry
func (m *Manager) ServeWS(w http.ResponseWriter, r *http.Request) {

	conn, err := m.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("failed to upgrade http client with the err: %v", err)
		return
	}

	// set the config values for the new connection
	conn.SetReadLimit(m.config.ReadLimitSize)
	_ = conn.SetReadDeadline(getDeadline(m.config.ReadDeadline))
	_ = conn.SetWriteDeadline(getDeadline(m.config.WriteDeadline))

	if m.authHandler != nil {
		if err = m.authHandler(r); err != nil {
			log.Printf("authentication failed for the user %v", conn.LocalAddr())
			w.WriteHeader(http.StatusUnauthorized)
			_ = conn.Close()
			return
		}
	}

	client := NewClient(conn, m)
	m.clients.Store(client.connIdentifier, client)

	// when the config is set to limit the connections from the same origin
	if m.config.LimitConnectionFromIP {
		// drop the connection if its from the same origin
		ipAddr := getClientIpAddr(r)
		if value, ok := m.clientsOrigin.LoadAndDelete(ipAddr); ok {
			oldClient, ok := value.(*Client)
			if !ok {
				log.Printf("invalid client type of %T in broadcast ", value)
				return
			}
			m.removeClient(oldClient)
		}
		m.clientsOrigin.Store(ipAddr, client)
	}
}

// Broadcast sends the message to all the connection currently connected to this manager.
// Message write is not guaranteed to be synchronous since the state is determined by the network and status of the
// connection
func (m *Manager) Broadcast(msg []byte) {
	m.clients.Range(func(_, value interface{}) bool {
		client, ok := value.(*Client)
		if !ok {
			log.Printf("invalid client type of %T in broadcast ", value)
			return true
		}
		client.writeMsg(msg, websocket.TextMessage)
		return true
	})
}

// Unicast sends a text message to the connection specified.
// returns ErrClientNotFound if the connIdentifier is invalid.
// returns ErrInvalidConnection if connection is invalid.
func (m *Manager) Unicast(connIdentifier string, msg []byte) error {
	client, err := m.loadClient(connIdentifier)
	if err != nil {
		return err
	}

	client.writeMsg(msg, websocket.TextMessage)
	return nil
}

// CloseConnection drops the connection specified
// returns ErrClientNotFound if the connIdentifier is invalid.
// returns ErrInvalidConnection if connection is invalid.
func (m *Manager) CloseConnection(connIdentifier string) error {
	client, err := m.loadClient(connIdentifier)
	if err != nil {
		return err
	}

	close(client.send)
	return nil
}

// loadClient wrapper function to load the connection from the manager.clients
func (m *Manager) loadClient(connIdentifier string) (*Client, error) {
	var ok bool
	var value any
	if value, ok = m.clients.Load(connIdentifier); !ok {
		return nil, ErrClientNotFound
	}

	client, ok := value.(*Client)
	if !ok {
		log.Printf("invalid client type of %T in broadcast ", value)
		return nil, ErrInvalidConnection
	}
	return client, nil
}

// removeClient removes the client to the client list of the manager
func (m *Manager) removeClient(client *Client) {
	m.clients.Delete(client.connIdentifier)

	if m.config.LimitConnectionFromIP {
		m.clientsOrigin.Delete(client.connIdentifier)
	}

	client.conn.Close()
}

// routeEvent routes the event triggered by the client to the registered function
func (m *Manager) routeEvent(event *proto.WebsocketEvent, client *Client) {
	eventHandler, ok := m.eventHandlers[event.GetEventType()]
	if !ok {
		log.Printf("unregistered event type %v triggered", event.GetEventType())
		return
	}

	event.ConnIdentifier = client.connIdentifier
	eventHandler(event)
}
