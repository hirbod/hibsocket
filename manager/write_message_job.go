package manager

// writeMessageJob is the struct used for sending messages to the conn.send channel for sending to the client
type writeMessageJob struct {
	body        []byte
	messageType int
}
