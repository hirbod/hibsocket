package manager

import (
	"net/http"
	"time"
)

// getClientIpAddr returns the real IP of the user, regardless of it being forwarded through a proxy
func getClientIpAddr(req *http.Request) string {
	clientIp := req.Header.Get("X-FORWARDED-FOR")
	if clientIp != "" {
		return clientIp
	}
	return req.RemoteAddr
}

func getDeadline(d time.Duration) time.Time {
	if d == 0 {
		return time.Time{}
	}
	return time.Now().Add(d)
}
