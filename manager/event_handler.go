package manager

import (
	proto "github.com/gravity-technologies/platform/backend/proto/go/websocket"
	"net/http"
)

type EventHandler func(event *proto.WebsocketEvent)
type AuthHandler func(req *http.Request) error
