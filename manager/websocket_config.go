package manager

import "time"

var (
	defaultConfig = &WebsocketConfig{
		ReadLimitSize:         1024,
		ReadDeadline:          time.Second * 0, // no deadline
		WriteDeadline:         time.Second * 0, // no deadline
		LimitConnectionFromIP: false,           // allow multiple connection from a single IP address
	}
)

// WebsocketConfig to be registered through Opts for the manager
type WebsocketConfig struct {
	// ReadLimitSize sets the ReadLimit in the websocket to block client from sending arbitrary big chunk of data
	// and overloading the server. If a message exceeds the limit, the connection sends a close message to the
	// peer and returns ErrReadLimit to the application
	// This must be set in accordance to the data object we expect to receive in the service layer
	ReadLimitSize int64

	// ReadDeadline sets the read deadline on the underlying network connection. After a read has timed out,
	// the websocket connection state is corrupt and all future reads will return an error.
	// A zero value for t means reads will not time out.
	ReadDeadline time.Duration

	// WriteDeadline sets the write deadline on the underlying network connection.
	// After a write has timed out, the websocket state is corrupt and all future writes will return an error.
	// A zero value for t means writes will not time out.
	WriteDeadline time.Duration

	// LimitConnectionFromIP limits number of connection from each IP address to 1. The old connection will
	// be dropped from the manager and replaced by the new one.
	LimitConnectionFromIP bool
}
