package manager

import "errors"

var (
	ErrClientNotFound    = errors.New("client not found")
	ErrInvalidConnection = errors.New("invalid connection")
)
